<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
error_reporting(E_ALL);
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
    session_destroy();
    header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>

    <form action="" method="post">
        <input name="login" />
        <input name="pass" />
        <input type="submit" value="Войти" />
    </form>

    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

    // TODO: Проверть есть ли такой логин и пароль в базе данных.
    // Выдать сообщение об ошибках.
    $user = 'u20386';
    $pass = '2551027';
    $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    // Если все ок, то авторизуем пользователя.
    try {
        $stmt = $db->prepare("SELECT id uid FROM app WHERE  login=:login AND pass=:pass");
        print($_POST['login']);
        print($_POST['pass']);
        $login = $_POST['login'];
        $password = md5($_POST['pass']);
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->bindParam(':pass', $password, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();

        if(empty($data)) {
            print('здесь empty');
            header('Location: ./login.php');
            exit();
        }
        else {
            print('здесь login');
            $_SESSION['login'] = $_POST['login'];
            // Записываем ID пользователя.
            $_SESSION['uid'] = $data[0]['uid'];
            print('uid' .  $data[0]['uid']);
            header('Location: ./');
            exit();
        }
    }
    catch(Exception $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    // Делаем перенаправление.

}

